on target:

pydeps ./usr/lib/python3.7/site-packages/mqttbrokerclient --show-dot --noshow --pylib

on host:

copy over the result to host as mqttbrokerclient.dot

dot -Tsvg mqttbrokerclient.dot -o mqttbrokerclient.svg
