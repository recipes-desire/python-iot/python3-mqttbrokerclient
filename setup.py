from setuptools import setup

install_requires = [
    "paho-mqtt==1.4.0",
]

setup(
    name='mqttbrokerclient',
    version='2.8.4',
    description=
    'A client that connects with an MQTT-Broker and exchanges IoT-messages',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com/recipes-desire/python-iot/python3-mqttbrokerclient',
    license='MIT license',
    packages=['mqttbrokerclient', 'mqttbrokerclient.mosquitto'],
    #package_data=[],
    #entry_points={
    #    'console_scripts': [
    #        'mqttbrokerclient_subscriber=mqttbrokerclient.simple_message_manager:main_subscriber',
    #        'mqttbrokerclient_publisher=mqttbrokerclient.simple_message_manager:main_publisher'
    #    ]
    #},
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: Messaging :: IoT :: Monitoring :: Broker :: Mosquitto :: Python-Client :: Paho',
    ],
    zip_safe=True)
