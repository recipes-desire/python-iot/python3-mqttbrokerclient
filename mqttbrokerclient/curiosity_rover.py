logger = None

broker = None


def launch(_logger, ip, port):
    global logger
    logger = _logger

    logger.debug("Mars Rover is Launched !")

    _broker = {
        "ip": ip,
        "port": port,
    }

    global broker
    broker = _broker


def get_logger():
    global logger
    return logger


def get_broker():
    global broker
    return broker
