import os

import paho.mqtt.publish as publish

from .mosquitto.connection import connect_client_blocking
from .messages.monitoring.host_discovery_msg import publish_msg_hosts_up

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def on_publish(client, userdata, mid):
    logger.debug("Message Published --> %s", client)
    pass


def create_topic_publish_message():
    # Read Environment Variables
    HOST = os.environ["MOSQUITTO_HOST"]
    PORT = os.environ["MOSQUITTO_PORT"]

    client = connect_client_blocking(HOST, int(PORT))

    client.loop_start()

    client.on_publish = on_publish

    hosts_array = ["host A", "host B", "host C", "host D"]
    msgs = publish_msg_hosts_up(hosts_array)

    #client.publish.multiple( msgs )
    # Publish Message #1
    client.publish("monitoring/hosts/up", "Miltos Run", qos=2)
    # Publish Message #2
    infot = client.publish("monitoring/hosts/up", "Miltos Walk", qos=2)

    infot.wait_for_publish()
