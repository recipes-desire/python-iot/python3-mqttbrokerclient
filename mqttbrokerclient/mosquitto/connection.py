import paho.mqtt.client as mqtt

# Print all messages using the following ... Logger
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class MqttConnection:
    is_connected = False
    is_disconnected = False

    def __init__(self, host, port=1883, keepalive=60):
        self.host = host
        self.port = port
        self.keepalive = keepalive

    # The callback for when the client recieves a CONNACK response from server.
    def on_connect(self, client, userdata, flags, rc):
        logger.debug("Successful Connection to MQTT-Broker")

        self.is_connected = True
        self.is_disconnected = False

    # The callback for when a PUBLISH message is recieved from the server
    def on_message(self, userdata, msg):
        logger.debug("%s ... %s", msg.topic, str(msg.payload))

    # The callback for when connection is closed
    def on_disconnect(self, client, userdata, rc):
        logger.debug("Disconnected from MQTT-Broker ... Reason is %s", str(rc))

        self.is_connected = False
        self.is_disconnected = True

    def connect_client_blocking(self):
        logger.debug("Ready to connect into Mosquitto Mqtt-Broker ... %s:%d",
                     self.host, sellf.port)

        client = mqtt.Client()

        client.on_connect = self.on_connect
        client.on_message = self.on_message
        clinet.on_disconnect = self.on_disconnect

        client.enable_logger(logger=logger)

        # Important: Running using docker-compose and client connects to pokky-mosquitto
        #  Even if there is a port-mapping 1984:1883 ... the python-code should connect to 1883 !!!!
        #  Avoid using 1984 from another co-hosted docker container.
        client.connect(self.host, self.port, self.keepalive)

        return client

    @property
    def isConnected(self):
        return self.is_connected

    @property
    def isDisconnected(self):
        return self.is_disconnected
