import os

from .simple_message_publisher import create_topic_publish_message
from .simple_message_subscriber import topics_receive_and_acknowledge


####
# Module Entrypoint ... Publisher and send a message for a Topic
def main_publisher():
    # Follow the PUBLISHER path ... send messages
    create_topic_publish_message()


####
# Module Entrypoint ... Subscriber consume messages on a Topic
def main_subscriber():
    # Follow the SUBSCRIBER path ... consume messages
    topics_receive_and_acknowledge()
