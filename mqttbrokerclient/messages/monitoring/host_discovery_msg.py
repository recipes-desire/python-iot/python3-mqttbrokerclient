def publish_msg_hosts_up(hosts_arr):

    topic = "monitoring/hosts/up"
    qos = 0
    retain = False

    msgs = []
    for host in hosts_arr:
        msgs.append((topic, host, qos, retain))

    return msgs
