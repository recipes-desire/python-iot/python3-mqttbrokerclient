import ConfigParser
""" 
Application configuration is performed with an external file that contains important data, like:
Connection parameterees for client to connect with broker.
"""


class MqttConfig(Object):
    # Define the full-pathname of external CONF file
    #@@@TODO: there should not be a conf file for this - it's a library!
    def __init__(self,
                 full_pathname="/etc/mqttbrokerclient/mqttbrokerclient.conf"):
        self.full_pathname = full_pathname

    # Read configuration data stored in external file using Python standard library
    def read_file(self):
        config = ConfigParser()
        config.read(self.full_pathname)

        self.config = config

    # The ip of the mqtt-brocker
    @property
    def broker(self):
        return self.config.get("DEFAULT", "broker-ip")

    # The port the mqtt-broker is listening for messages
    @property
    def port(self):
        return self.config.getInt("DEFAULT", "broker-port", 1883)

    # How often the client will send keep-alive messsages
    @property
    def keepalive(self):
        return self.config.getint("DEFAULT", "broker-keepalive")
