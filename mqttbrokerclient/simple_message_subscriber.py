import paho.mqtt.subscribe as subscribe

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def topics_receive_and_acknowledge():
    topics = ["monitoring/hosts/up"]

    m = subscribe.simple(topics,
                         hostname="poky-mosquitto",
                         retained=False,
                         msg_count=2)

    for a in m:
        logger.debug(a.topic)
        logger.debug(a.payload)
